/* YourDuino Multiple DS18B20 Temperature Sensors on 1 wire
  Connections:
  DS18B20 Pinout (Left to Right, pins down, flat side toward you)
  - Left   = Ground
  - Center = Signal (Pin 2):  (with 3.3K to 4.7K resistor to +5 or 3.3 )
  - Right  = +5 or +3.3 V

   Questions: terry@yourduino.com 
   V1.01  01/17/2013 ...based on examples from Rik Kretzinger
   
/*-----( Import needed libraries )-----*/
// Get 1-wire Library here: http://www.pjrc.com/teensy/td_libs_OneWire.html
#include <OneWire.h>
#include <dht.h>
dht DHT;

//Get DallasTemperature Library here:  http://milesburton.com/Main_Page?title=Dallas_Temperature_Control_Library
#include <DallasTemperature.h>

/*-----( Declare Constants and Pin Numbers )-----*/
#define ONE_WIRE_BUS_PIN 2
#define DHT11_PIN 7

/*-----( Declare objects )-----*/
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS_PIN);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

/*-----( Declare Variables )-----*/
// Assign the addresses of your 1-Wire temp sensors.
// See the tutorial on how to obtain these addresses:
// http://www.hacktronics.com/Tutorials/arduino-1-wire-address-finder.html

DeviceAddress Probe01 = { 0x28, 0xFF, 0x32, 0xD4, 0xC1, 0x16, 0x04, 0x9F }; 
DeviceAddress Probe02 = { 0x28, 0xFF, 0x3F, 0x12, 0xC0, 0x16, 0x05, 0xB6 };
DeviceAddress Probe03 = { 0x28, 0xFF, 0x5A, 0x87, 0xB5, 0x16, 0x03, 0x12 };
//DeviceAddress Probe04 = { 0x28, 0x9A, 0x80, 0x40, 0x04, 0x00, 0x00, 0xD5 };
//DeviceAddress Probe05 = { 0x28, 0xE1, 0xC7, 0x40, 0x04, 0x00, 0x00, 0x0D };


void setup()   /****** SETUP: RUNS ONCE ******/
{
  // start serial port to show results
  Serial.begin(9600);
  Serial.print("Initializing Temperature Control Library Version ");
  Serial.println(DALLASTEMPLIBVERSION);
  
  // Initialize the Temperature measurement library
  sensors.begin();
  
  // set the resolution to 10 bit (Can be 9 to 12 bits .. lower is faster)
  sensors.setResolution(Probe01, 12);
  sensors.setResolution(Probe02, 12);
  sensors.setResolution(Probe03, 12);
  //sensors.setResolution(Probe04, 10);
  //sensors.setResolution(Probe05, 10);
  
}//--(end setup )---
  

void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  char buffer[6];
  //Serial.println(sensors.getDeviceCount());   
  //Serial.print("Getting temperatures... "); 
  //Serial.print("Number of Devices found on bus = ");   
  //Serial.println(); 
  float chk = DHT.read11(DHT11_PIN);
  
  //Serial.println();
   
  // Command all devices on bus to read temperature  
  sensors.requestTemperatures();  
  
  Serial.print("#S|TEMPS|[,");
  printTemperature(Probe01);
  Serial.print(",");
  printTemperature(Probe02);
  Serial.print(",");
  printTemperature(Probe03);
  Serial.print(",");
  Serial.print(itoa((DHT.temperature*1000),buffer,10));
  Serial.print(",");
  Serial.print(itoa((DHT.humidity*1000),buffer,10));
  Serial.println("]#");
   
  delay(10000);
  
}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/
void printTemperature(DeviceAddress deviceAddress)
{

float tempC = sensors.getTempC(deviceAddress);
char buffer[8];
   if (tempC == -127.00) 
   {
   Serial.print("Error getting temperature  ");
   } 
   else
   {
   tempC = tempC*1000;
   Serial.print(itoa((tempC),buffer,10));
   
   }
}// End printTemperature
//*********( THE END )***********
