Welcome to the project home for any code that I use/used in NeXtRAD. Some of the phase code is located in another project file called 'Phase_Stability'. This project was made in preperation of experimental trials of our UCT/UCL multistatic radar called NeXtRAD. References to everything I do is usually contained in the write-up for each doc, which will be place in the documentation sub-folder.

All code is freely available to chop and change as you see fit, but if you do use any of it, it would be appreciated if you could reference my work/the work of my collaborators =)

Quick guide:

onewire2oghum.ino is my Arduino code for measuring the temperature and humidity from 3 DS18B20 temperature sensors and 1 DHT11 temperature and humidity sensors. The basic 'design' (very crude, quick overview really) is in Appendix B of the 'December_Trials' document.


